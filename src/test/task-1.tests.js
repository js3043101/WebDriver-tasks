const { expect, browser, $ } = require('@wdio/globals');

describe('task 1', () => {
    it('should create a new paste with task 1', async () => {
        const expiration = await $('#select2-postform-expiration-container');
        await browser.url('https://pastebin.com/');
        await $('#postform-text').setValue('Hello from WebDriver');
        await expiration.click();
        
        await browser.performActions([{
            type: 'key',
            id: 'keyboard',
            actions: [
            {type: "pause", duration: 1000},
            {type: 'keyDown', value: "\ue015"},
            {type: "pause", duration: 1000},
            {type: 'keyDown', value: "\ue015"}
            ]
        }]);
        await browser.keys("Enter");
        await $('#postform-name').setValue('helloweb');
        await $("button[class='btn -big']").click();
    })
})

