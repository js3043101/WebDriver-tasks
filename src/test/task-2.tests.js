const { expect, browser, $ } = require('@wdio/globals')

describe('Task 2', () => {
    it('should create a new paste with task 2', async () => {
        const code = 'git config --global user.name  "New Sheriff in Town"' + '\n' + 'git reset $(git commit-tree HEAD^{tree} -m "Legacy code")' + '\n' + 'git push origin master --force'
        const expiration = await $('#select2-postform-expiration-container');
        await browser.url('https://pastebin.com/');
        await $('#postform-text').setValue(code);
    
        //Bash
        await $('#select2-postform-format-container').click();
        await $("input[role='searchbox']").setValue('Bash');
        browser.keys("Enter");
        
        await expiration.click();
        await browser.performActions([{
            type: 'key',
            id: 'keyboard',
            actions: [
            {type: "pause", duration: 1000},
            {type: 'keyDown', value: "\ue015"},
            {type: "pause", duration: 1000},
            {type: 'keyDown', value: "\ue015"}
            ]
        }]);
        await browser.keys("Enter");

        await $('#postform-name').setValue('how to gain dominance among developers');
        await $("button[class='btn -big']").click();
        const tittle = await $("div[class='info-top'] h1");
        await expect(tittle).toHaveText('how to gain dominance among developers');
        const Syntax = await $("(//a[normalize-space()='Bash'])[1]");
        await expect(Syntax).toHaveText('Bash');
        await $("//a[normalize-space()='raw']").click();
        const code2 = await $("(//body)[1]");
        
        await code2.waitForDisplayed();
        await browser.pause(9000);
        await expect(code2).toHaveText(code);

        
    })
})

